## About Liteboard

Liteboard will become a very simplistic discussion board. It's main purpose is for me to learn the basics of the Laravel PHP framework and become familiar with it. After this I should be ready to take on more complex projects.

# Planed Features
- User Registration with confirmation email.
- Password Reset
- Admin and Users
- Basic User Profiles
- Forum Categories
- Sub-Categories
- Threads
- Posibillity to remove threads in a given time period.

# Maybe in the Future
- PMs
- Moderation
- WYSIWYG Editor

# Important notice
This project is not intended for constructive use. I cannot and will not offer any kind of technical support.

# Used Frameworks
- Laravel
